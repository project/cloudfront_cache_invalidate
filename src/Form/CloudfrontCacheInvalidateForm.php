<?php

namespace Drupal\cloudfront_cache_invalidate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Psr\Log\LoggerInterface;

/**
 * Configure Cloudfront Cache Invalidate for this site.
 */
class CloudfrontCacheInvalidateForm extends ConfigFormBase {
  use StringTranslationTrait;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new Cloudfront Cache clear form object.
   */
  public function __construct(
    MessengerInterface $messenger,
    LoggerInterface $logger
  ) {
    $this->messenger = $messenger;
    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('logger.factory')->get('cloudfront_cache_invalidate')
    );
  }

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'cci_Cloudfront_cache.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cci_Cloudfront_cache_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $type_options = [
      'cloudfront' => 'Cloudfront',
    ];
    $cache_type_checkbox = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Cache Type'),
      '#options' => $type_options,
    ];

    if ($config->get('cache_type')) {
      $cache_type_checkbox['#default_value'] = $config->get('cache_type');
    }

    $form['cache_type'] = $cache_type_checkbox;

    $cloudfront_url_array = [
      '#type' => 'textarea',
      '#title' => $this->t('Cloudfront URL to invalidate'),
      '#description' => $this->t('Specify the existing path you wish to invalidate. For example: /sector/*, /state/*. Enter one value per line'),
      '#placeholder' => 'Cloudfront UR',
      '#states' => [
        'visible' => [
          ':input[name="cache_type[cloudfront]"]' => [
              ['checked' => TRUE],
          ],
        ],
        'required' => [
          ':input[name="cache_type[cloudfront]"]' => [
              ['checked' => TRUE],
          ],
        ],
      ],
    ];

    if ($config->get('cloudfront_url')) {
      $cloudfront_url_array['#default_value'] = $config->get('cloudfront_url');
    }

    $form['cloudfront_url'] = $cloudfront_url_array;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Get the URL.
    $url_value = explode("\n", $form_state->getValue('cloudfront_url'));

    if (!empty($url_value) && is_array($url_value) && count($url_value) > 0) {
      foreach ($url_value as $value) {
        if (substr($value, 0, 1) != '/' && !empty($value)) {
          $form_state->setErrorByName('url', $this->t('The Cloudfront URL introduced is not valid.'));
        }
      }
    }

    else {
      $form_state->setErrorByName('url', $this->t('The Cloudfront URL introduced is not valid.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $config_type = $form_state->getValue('cache_type');

      if (isset($config_type['cloudfront']) && $config_type['cloudfront']) {
        $url_value = explode("\n", $form_state->getValue('cloudfront_url'));
        // Get the Paths.
        $paths = [];
        foreach ($url_value as $value) {
          if ($value) {
            $paths[] = trim($value);
          }
        }

        // Invalidate URL.
        list($status, $message) = cloudfront_cache_invalidate_url($paths);

        if ($status === TRUE) {
          $this->messenger->addMessage('Cloudfront Cache invalidation is in progress.', $this->messenger::TYPE_STATUS);
        }
        else {
          $this->messenger->addMessage($message, $this->messenger::TYPE_ERROR);
        }
      }
    }
    catch (\Exception $e) {
      $this->logger->error('Message from @module: @message.', [
        '@module' => 'cloudfront_cache_invalidate',
        '@message' => $e->getMessage(),
      ]);
      $this->messenger->addMessage($e->getMessage(), $this->messenger::TYPE_ERROR);
    }

  }

}
